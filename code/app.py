from flask import Flask, g
import sqlite3

DATABASE = 'database.db'


app = Flask(__name__)
API_ROOT = 'api/v1.0'


def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db


if __name__ == '__main__':
    app.run(debug=True)
