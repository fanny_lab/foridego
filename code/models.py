from flask_sqlalchemy import SQLAlchemy

from code.app import app

db = SQLAlchemy(app)


class Employee(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstName = db.Column(db.String(100), unique=True, nullable=False)
    lastName = db.Column(db.String(100), unique=True, nullable=False)

    def __repr__(self):
        return '{0} {1}'.format(self.firstName, self.lastName)
