from flask import abort, jsonify, request

from code.app import app, API_ROOT
from code.models import Employee


@app.route('/{0}/'.format(API_ROOT), methods=['GET'])
def home():
    return 'HomePage'


@app.route('/{0}/emploees'.format(API_ROOT), methods=['GET', 'POST'])
def manage_emploees():
    if request.method == 'POST':
        firstName = str(request.data.get('firstName', ''))
        lastName = str(request.data.get('lastName', ''))
        if firstName and lastName:
            employee = Employee(firstName=firstName, lastName=lastName)
            employee.save()
            response = jsonify({'employee': employee})
            response.status_code = 201
        else:
            response = jsonify({'error': 'Bad data'})
            response.status_code = 400
    else:
        emploees = Employee.get_all()
        response = jsonify({'employees': emploees})
        response.status_code = 200
    return response


@app.route('{0}/emploee/<int:empl_id>'.format(API_ROOT), methods=['DELETE', 'PUT'])
def manage_employee(empl_id):
    emploee = Employee.query.filter_by(id=empl_id).first()
    if not emploee:
        abort(404)
    if request.method == 'DELETE':
        emploee.delete()
        response = {'message': 'deleted user'}
        response.status_code = 204
    else:
        lastName = str(request.data.get('lastName', ''))
        if lastName:
            emploee.lastName = lastName
            emploee.save()
            response = jsonify({'employee': emploee})
            response.status_code = 200
    return response
